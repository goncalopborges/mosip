import java.util.Random;

public class Event {
    private static final Random RANDOM = new Random(System.currentTimeMillis());

    // Seasonality - Clock reference        
    static final int MAY_JUNE = 172800; //1st of May - 5% more flights
    static final int JULY_AUGUST = 259200; //1st of July - 10% more flights
    static final int SEPTEMBER = 345600; //1st of September - 5% more flights
    static final int OCTOBER = 388800; //1st of October - normality
    
    private EventType eventType;
    private double time;

	public Event(EventType eventType, double clock) {
		this.eventType = eventType;
		
		if (this.eventType == EventType.FINISH_TAKEOFF || this.eventType == EventType.FINISH_LANDING
				|| clock <= MAY_JUNE || clock >= OCTOBER) {
			
			this.time = clock + this.calculateTimeForEvent(0.0);
			
		} else if (clock > MAY_JUNE && clock < JULY_AUGUST) {
			
			this.time = clock + this.calculateTimeForEvent(-0.1);
			
		} else if (clock >= JULY_AUGUST && clock < SEPTEMBER) {
			
			this.time = clock + this.calculateTimeForEvent(-0.2);
			
		} else if (clock >= SEPTEMBER && clock < OCTOBER) {
			
			this.time = clock + this.calculateTimeForEvent(-0.1);
			
		}
	}

    public EventType getEventType() {
        return eventType;
    }

    public double getTime() {
        return time;
    }
    
    public void setTime (int time) {
    	this.time=time;
    }

    /**
     * Calculates the time for the event to take place.
     * This value is generated randomly using a gaussian distribution, modified with 4 extra parameters:
     * <pre>- Min: The minimum value of the distribution;</pre>
     * <pre>- Max: The maximum value of the distribution;</pre>
     * <pre>- Skew: The degree to which the values cluster around the mode of the distribution. Higher values imply tighter clustering;</pre>
     * <pre>- Bias: The tendency for the distribution to approach the minimum or maximum values. Positive values tend towards the max, negative values tend towards the min.</pre>
     *
     * @return Time of the event.
     */
    private double calculateTimeForEvent(double biasModifier) {
        double range = eventType.getMax() - eventType.getMin();
        double mid = eventType.getMin() + range / 2.0;
        double unitGaussian = RANDOM.nextGaussian();
        double biasFactor = Math.exp(eventType.getBias() + biasModifier);

        return mid + (range * (biasFactor / (biasFactor + Math.exp(-unitGaussian / eventType.getSkew())) - 0.5));
    }
}
