import java.util.*;

/**
 * All time units used in the application are in minutes.
 * The only exception is the duration of the simulation entered by the user, which is in days.
 */
public class Main {

    static double clock;
    static int nrLeavingAirplanesWaiting;
    static int nrArrivingAirplanesWaiting;
    static int simulationDuration;
    static AirstripState airstripState;
    static Queue<Event> events;

    // Statistic variables
    static double lastEventTime;
    static double lastAirstripOccupationRequestTime;
    static double totalTimeBetweenAirstripOccupationRequests;
    static int nrCompletedFlights;
    static int nrLeavingAirplanes;
    static int nrArrivingAirplanes;
    static int nrLeavingAirplanesDelayed;
    static int nrArrivingAirplanesDelayed;
    static double totalWaitingTimeOfLeavingAirplanes;
    static double totalWaitingTimeOfArrivingAirplanes;
    static double totalTimeAirstripBusy;
    
    //Simulation duration
    static final int YEAR_DURATION_IN_DAYS = 365;
    static final int TWO_YEARS_DURATION_IN_DAYS = 730;
    static final int FIVE_YEARS_DURATION_IN_DAYS = 1825;
    static final int DECADE_DURATION_IN_DAYS = 3650;
    
    static {
        airstripState = AirstripState.FREE;
        events = new PriorityQueue<>(Comparator.comparingDouble(Event::getTime));
        simulationDuration = YEAR_DURATION_IN_DAYS;
    }

    public static void main(String[] args) {
        System.out.print("Duration of the simulation (in days): 365");

        upcomingLeavingAirplane();
        upcomingArrivingAirplane();

        while(!isSimulationOver()) {
            Event nextEvent = events.poll();
            clock = nextEvent.getTime();

            updateStatistics();
            handleEvent(nextEvent);
        }

        generateSimulationReport();
    }

    private static boolean areArrivingAirplanesWaiting() {
        return nrArrivingAirplanesWaiting > 0;
    }

    private static boolean areLeavingAirplanesWaiting() {
        return nrLeavingAirplanesWaiting > 0;
    }

    private static boolean isSimulationOver() {
        return clock > (simulationDuration * 24 * 60);
    }

    private static void upcomingLeavingAirplane() {
        addNewEvent(EventType.REQUEST_FOR_TAKEOFF);
    }

    private static void upcomingArrivingAirplane() {
        addNewEvent(EventType.REQUEST_FOR_LANDING);
    }

    private static void takeoffAirplane() {
        addNewEvent(EventType.FINISH_TAKEOFF);
        airstripState = AirstripState.BUSY;
        nrLeavingAirplanesWaiting--;
    }

    private static void landAirplane() {
        addNewEvent(EventType.FINISH_LANDING);
        airstripState = AirstripState.BUSY;
        nrArrivingAirplanesWaiting--;
    }

    private static void addNewEvent(EventType eventType) {
        Event event = new Event(eventType, clock);
        events.add(event);
    }

    private static void handleEvent(Event event) {
        EventType eventType = event.getEventType();

        if (eventType == EventType.REQUEST_FOR_TAKEOFF) {
            handleLeavingAirplaneArrival();
        } else if (eventType == EventType.REQUEST_FOR_LANDING) {
            handleArrivingAirplaneArrival();
        } else if (eventType == EventType.FINISH_TAKEOFF || eventType == EventType.FINISH_LANDING) {
            handleFinishedTakeoffOrLanding();
        }
    }

    private static void handleLeavingAirplaneArrival() {
        nrLeavingAirplanes++;
        nrLeavingAirplanesWaiting++;

        totalTimeBetweenAirstripOccupationRequests += clock - lastAirstripOccupationRequestTime;
        lastAirstripOccupationRequestTime = clock;

        if (airstripState == AirstripState.FREE) {
            takeoffAirplane();
        } else {
            nrLeavingAirplanesDelayed++;
        }

        upcomingLeavingAirplane();
    }

    private static void handleArrivingAirplaneArrival() {
        nrArrivingAirplanes++;
        nrArrivingAirplanesWaiting++;

        totalTimeBetweenAirstripOccupationRequests += clock - lastAirstripOccupationRequestTime;
        lastAirstripOccupationRequestTime = clock;

        if (airstripState == AirstripState.FREE) {
            landAirplane();
        } else {
            nrArrivingAirplanesDelayed++;
        }

        upcomingArrivingAirplane();
    }

    private static void handleFinishedTakeoffOrLanding() {
        airstripState = AirstripState.FREE;
        nrCompletedFlights++;

        // Airplanes arriving at the airport have priority over airplanes leaving the airport
        if(areArrivingAirplanesWaiting()) {
            landAirplane();
        } else if (areLeavingAirplanesWaiting()) {
            takeoffAirplane();
        }
    }

    private static void updateStatistics() {
        double timeSinceLastEvent = clock - lastEventTime;
        totalWaitingTimeOfArrivingAirplanes += nrArrivingAirplanesWaiting * timeSinceLastEvent;
        totalWaitingTimeOfLeavingAirplanes += nrLeavingAirplanesWaiting * timeSinceLastEvent;

        if (airstripState == AirstripState.BUSY) {
            totalTimeAirstripBusy += timeSinceLastEvent;
        }

        lastEventTime = clock;
    }

    private static void generateSimulationReport() {
        System.out.println("\n\n============================ SIMULATION REPORT ===============================\n\n");

        System.out.println("GENERAL STATISTICS:\n");
        System.out.printf("- Average Number of Flights per Day: %.2f%n", ((double) (nrLeavingAirplanes + nrArrivingAirplanes) / simulationDuration));
        System.out.printf("- Average Airstrip Occupation Time per Flight: %.2f minutes%n", totalTimeAirstripBusy / nrCompletedFlights);
        System.out.printf("- Average Time Between Airstrip Occupation Requests (Landing / Takeoff): %.2f minutes%n", (totalTimeBetweenAirstripOccupationRequests / (nrLeavingAirplanes + nrArrivingAirplanes)));
        System.out.printf("- Percentage of Time Airstrip was Busy: %.2f%%%n", (totalTimeAirstripBusy / clock) * 100);

        System.out.println("\n\nARRIVING AIRPLANES STATISTICS:\n");
        System.out.printf("- Total Number of Airplanes: %d%n", nrArrivingAirplanes);
        System.out.printf("- Percentage of Airplanes Delayed: %.2f%%%n", ((double) nrArrivingAirplanesDelayed / nrArrivingAirplanes) * 100);
        System.out.printf("- Average Delay Time: %.2f minutes%n", totalWaitingTimeOfArrivingAirplanes / nrArrivingAirplanes);
        System.out.printf("- Average Number of Airplanes Waiting to Land at a given Time: %.2f%n", totalWaitingTimeOfArrivingAirplanes / clock);

        System.out.println("\n\nLEAVING AIRPLANES STATISTICS:\n");
        System.out.printf("- Total Number of Airplanes: %d%n", nrLeavingAirplanes);
        System.out.printf("- Percentage of Airplanes Delayed: %.2f%%%n", ((double) nrLeavingAirplanesDelayed / nrLeavingAirplanes) * 100);
        System.out.printf("- Average Delay Time: %.2f minutes%n", totalWaitingTimeOfLeavingAirplanes / nrLeavingAirplanes);
        System.out.printf("- Average Number of Airplanes Waiting to Takeoff at a given Time: %.2f%n", totalWaitingTimeOfLeavingAirplanes / clock);
    }
}