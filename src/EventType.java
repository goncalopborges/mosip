public enum EventType {
    REQUEST_FOR_TAKEOFF(0, 80, 1.2, -0.35),
    FINISH_TAKEOFF(8, 23, 2.5, -0.2),
    REQUEST_FOR_LANDING(0, 80, 1.1, -0.3),
    FINISH_LANDING(10, 25, 2, -0.6);

    private double min;
    private double max;
    private double skew;
    private double bias;

    EventType(double min, double max, double skew, double bias) {
        this.min = min;
        this.max = max;
        this.skew = skew;
        this.bias = bias;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getSkew() {
        return skew;
    }

    public double getBias() {
        return bias;
    }
}
